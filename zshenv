#===============================================================================
#
#         FILE: .zshenv
#
#        USAGE: - 
#
#
#  DESCRIPTION: ZSH configuration
#
#      OPTIONS: -
# REQUIREMENTS: -
#         BUGS: -
#        NOTES: Personal settings only
#       AUTHOR: egon@gruenter.eu
#      COMPANY: -
#      VERSION: 0.1
#      CREATED: October 16h 2020
#     REVISION: -
#===============================================================================

export EDITOR=vi
export PATH=$HOME/bin:$PATH