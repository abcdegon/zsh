#===============================================================================
#
#         FILE: .zshrc
#
#        USAGE: - 
#
#
#  DESCRIPTION: ZSH configuration
#
#      OPTIONS: -
# REQUIREMENTS: -
#         BUGS: -
#        NOTES: Personal settings only
#       AUTHOR: egon@gruenter.eu
#      COMPANY: -
#      VERSION: 0.1
#      CREATED: October 16h 2020
#     REVISION: -
#===============================================================================

#-------------------------------------------------------------------------------
# PS1 is the primary prompt string. It should be set on any interactive shell.
# If PS1 is empty then stop running this script, else set PS1 and PS2.
# 
# ec:1   21:12:29  [user@host:~]
# start cmd:>...
# cont. cmd:>
#-------------------------------------------------------------------------------
[ -z "$PS1" ] && return
PS1="--------------------------------------------------------------------------------\nec:$(printf %-3d $?) \t  [\u@\h:\w]\nstart cmd:> "
PS2="cont. cmd:> "

#-------------------------------------------------------------------------------
# set my personal aliases
#-------------------------------------------------------------------------------
alias .. = 'cd ..'
alias ll = 'ls -lisa'
